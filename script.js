function form(){
	this.currentID=0;
	this.numberItemsLeft=0;
	this.checkedItems=0;
	this.lastNode;
	var self=this;
	this.begin=function(){
		var input=document.getElementById("input");
		input.value="";
		var deleteAll=document.getElementById("deleteAll");
		input.addEventListener("keypress",this.addNew);
		deleteAll.addEventListener("click",this.deleteAll);
	};
	
	this.addNew=function(e){
		if(e.keyCode!=13) return false;
		var toDoList=document.getElementById("todoList");

		var inPut=document.getElementById("input");
		//Create new elements
		var span=document.createElement("span");
		var content=document.createTextNode(inPut.value);
		span.appendChild(content);
		var divElement=document.createElement("div");
	 	var checkBox=document.createElement("input");
	 	var removeBtn=document.createElement("input");
	 	//Modify checkbox
	 	checkBox.type="checkbox";
	 	checkBox.value="item"+this.currentID;
	 	checkBox.className="itemCheckBox";
	 	checkBox.addEventListener("click",function(){
	 		if(checkBox.checked===true)
	 		{
	 			self.numberItemsLeft--;
	 			self.checkedItems++;
	 			self.updateItemLeftNumber();
	 			self.updateDeleteAllButton()
	 			self.addClass(span,"deleted");
	 		}
	 		else
	 		{
	 			self.numberItemsLeft++;
	 			self.checkedItems--;
	 			self.updateItemLeftNumber();
	 			self.updateDeleteAllButton()
	 			self.removeClassDeleted(span);
	 		}
 		});
 		//Modify the remove button
	 	removeBtn.type="button";
	 	removeBtn.value="Remove";
	 	removeBtn.className="item"+self.currentID;
	 	removeBtn.addEventListener("click",function(){
	 		toDoList.removeChild(divElement);
	 		if(checkBox.checked===false)
	 		{
	 		self.numberItemsLeft--;
	 		self.updateItemLeftNumber();
	 		}
	 	});
	 	//Modify div element
	 	divElement.className="item"+self.currentID;
	 	divElement.addEventListener("mousedown", function(e){
	 		self.lastNode=e.target.parentNode;
	 		if(self.lastNode.id==="todoList") //Just get the div element
	 		{
	 			self.lastNode=e.target;
	 		}
	 	});
	 	divElement.addEventListener("mouseup", function(e){
	 		var targetNode=e.target;
	 		if(targetNode.id==="todoList")
	 		{
	 			targetNode=e.target;	 //Just get the div element
	 		}
	 		targetNode.parentNode.insertBefore(self.lastNode,targetNode);
	 	});
	 	//Add item to their place
	 	divElement.appendChild(checkBox);
		divElement.appendChild(span);
		divElement.appendChild(removeBtn);
		//Add new item to to do list
		toDoList.appendChild(divElement);
		inPut.value="";
		self.currentID++;
		self.numberItemsLeft++;
		self.updateItemLeftNumber();
	};

	this.updateItemLeftNumber=function(){
		if(self.numberItemsLeft===0)
		{
			document.getElementById("numberItems").className="hidden";
		}
		else
		{
			numberItemTag=document.getElementById("numberItems");
			numberItemTag.className="";
			numberItemTag.innerHTML=self.numberItemsLeft+" items left";
		}
	};

	this.updateDeleteAllButton=function(){
		if(self.checkedItems===0)
		{
		var deleteAllButton=document.getElementById("deleteAll");
		deleteAllButton.className="hidden";
		}
		else
		{
		var deleteAllButton=document.getElementById("deleteAll");
		deleteAllButton.className="";
		if(self.checkedItems>1)
			var text="Delete all "+self.checkedItems+" items";
		else
			text="Delete "+self.checkedItems+" item";
		deleteAllButton.value=text;

		}
	};

	this.thunghiem=function(){
		window.alert("Thu nghiem");
	};

	this.addClass=function(item,classString){
		item.className=item.className+classString;
	};
	this.removeClassDeleted=function(item){
		item.className="normalItem";
	};
	this.deleteAll=function(){

		var itemCheckList=document.getElementsByClassName("itemCheckBox");
		var toDoList=document.getElementById("todoList");
		for(var i=0; i<itemCheckList.length;i++)
		{

			if(itemCheckList[i].checked===true)
			{

				var item=itemCheckList[i].parentNode;
				toDoList.removeChild(item);
				i--;
				self.checkedItems--;
			}
		}
		self.updateDeleteAllButton();
	};
}


var newOne=new form();
newOne.begin();